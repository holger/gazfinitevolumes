#include <iostream>
#include "BasisKlassen/trunk/multiArray.hpp"

typedef BK::MultiArray<2,<BK::Array<double,2>>> Matrix;

int main()
{
  enum {rho, mx, my};
  
  int nx = 10;
  int ny = 10;
  double lambda = 2*M_PI;
  double lx = lambda + dx;
  double ly = lambda + dy;

  int nt = 10;
  double cfl = 0.1;
  double dt = cfl*dx;
  double a = 0.5;

  std::cerr << "dt = " << dt << std::endl;
  
  Matrix var(nx, ny);
  Matrix fmx(nx, ny);
  Matrix fmy(nx, ny);
  Matrix varm(nx-1, ny-1);
  Matrix varp(nx-1, ny-1);
  
  for(int i=0;i<nx;i++)
    for(int j=0;j<ny;j++) {
      var(i, j)[rho] = 1;
      var(i, j)[mx] = sin(2*M_PI/lambda * (i+0.5)*dx);
      var(i, j)[my] = 0;
    }

  std::ofstream out;
  out.open("rho0.data");
  for(int i=0;i<nx;i++) {
    for(int j=0;j<ny;j++) 
      out << "\t" << var(i,j)[rho];
    out << std::endl;
  }
  out.close();
  
  for(int t=0; t<nt;t++) {
    if(t%10 == 0) {

      out.open(BK::toString("rho-",t,".data"));
      for(int i=0;i<nx;i++) {
	for(int j=0;j<ny;j++) 
	  out << "\t" << var(i,j)[rho];
	out << std::endl;
      }
      out.close();
    }

    for(int i=1; i<nx-1; i++) 
      for(int j=1; j<ny-1; j++) {
	fmx(i,j)[rho] = var(i,j)[mx];
	fmx(i,j)[mx] = var(i,j)[mx]*var(i,j)[mx]/var(i,j)[rho] + a*a*var(i,j)[rho];
	fmx(i,j)[my] = var(i,j)[mx]*var(i,j)[my]/var(i,j)[rho];

	fmy(i,j)[rho] = var(i,j)[my];
	fmy(i,j)[mx] = var(i,j)[my]*var(i,j)[mx]/var(i,j)[rho];
	fmy(i,j)[my] = var(i,j)[mx]*var(i,j)[mx]/var(i,j)[rho] + a*a*var(i,j)[rho];

	fpx(i,j)[rho] = var(i+1,j)[mx];
	fpx(i,j)[mx] = var(i+1,j)[mx]*var(i+1,j)[mx]/var(i+1,j)[rho] + a*a*var(i+1,j)[rho];
	fpx(i,j)[my] = var(i+1,j)[mx]*var(i+1,j)[my]/var(i+1,j)[rho];

	fpy(i,j)[rho] = var(i,j+1)[my];
	fpy(i,j)[mx] = var(i,j+1)[my]*var(i,j+1)[mx]/var(i,j+1)[rho];
	fpy(i,j)[my] = var(i,j+1)[mx]*var(i,j+1)[mx]/var(i,j+1)[rho] + a*a*var(i,j+1)[rho];

	for(int v=0;v<3;v++) {
	  varm(i,j)[v] = var(i,j)[v];
	  varp(i,j)[v+1] = var(i,j)[v+1];
	}
      }
    
    for(int i=1; i<nx-1; i++) {
      for(int j=1; j<ny-1; j++) {
	fi[i] = laxFriedrichs1d_flux_numeric(fmx[i-1], fpx[i-1], dfmx[i-1], dfp[i-1], qm[i-1], qp[i-1]);
	fo[i] = laxFriedrichs1d_flux_numeric(fm[i], fp[i], dfm[i], dfp[i], qm[i], qp[i]);
      }

	
	
      }
  }
